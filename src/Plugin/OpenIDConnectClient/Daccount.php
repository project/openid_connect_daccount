<?php

namespace Drupal\openid_connect_daccount\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\GeneratedUrl;
use Drupal\Core\Url;
use Drupal\openid_connect\Plugin\OpenIDConnectClientBase;

/**
 * OpenID Connect client for d ACCOUNT connect.
 *
 * Used to log in to Drupal sites using d ACCOUNT as authentication provider.
 *
 * @OpenIDConnectClient(
 *   id = "daccount",
 *   label = @Translation("d ACCOUNT")
 * )
 */
class Daccount extends OpenIDConnectClientBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form_state->setCached(FALSE);

    $form['#attached']['library'][] = 'openid_connect_daccount/openid_connect_daccount_common';

    $form['iss_allowed_domains']['#access'] = FALSE;

    $form['d_account_type'] = [
      '#title' => $this->t('Types of d ACCOUNT connect'),
      '#type' => 'radios',
      '#options' => [
        'd_account' => $this->t('d ACCOUNT connect'),
        'business_d_account' => $this->t('Business d ACCOUNT connect'),
      ],
      '#default_value' => !empty($this->configuration['d_account_type']) ? $this->configuration['d_account_type'] : 'business_d_account',
      '#description' => $this->t('Choose whether to use d ACCOUNT connect or Business d ACCOUNT connect.'),
      '#weight' => -10,
      '#require' => TRUE,
    ];

    $form['scopes'] = [
      '#title' => $this->t('Scopes'),
      '#type' => 'textfield',
      '#description' => $this->t('Custom scopes, separated by spaces. For example: openid email_n'),
      '#default_value' => !$this->configuration['scopes'] ?: implode(' ', $this->configuration['scopes']),
    ];

    $form['authif'] = [
      '#title' => $this->t('Enable authentication for customers beyond DOCOMO'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['authif'],
      '#description' => $this->t('If enabled, customers outside of DOCOMO can also be authenticated. If disabled, authentication is restricted to contractors.'),
    ];

    $form['dummy_email_enable'] = [
      '#title' => $this->t('Use a dummy username and email address'),
      '#type' => 'checkbox',
      '#default_value' => $this->configuration['dummy_email_enable'],
      '#description' => $this->t('If you enable this setting, a dummy username and email address will replace the ID and email address registered with your d ACCOUNT.'),
    ];

    $form['dummy_email_domain'] = [
      '#title' => $this->t('Domain of dummy email address'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['dummy_email_domain'],
      '#description' => $this->t("Specify the domain for the dummy email address that you want to register as the user's email address."),
      '#field_prefix' => '@',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $configuration = $form_state->getValues();

    if (!empty($configuration['scopes'])) {
      $this->setConfiguration(['scopes' => explode(' ', $configuration['scopes'])]);
    }

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    parent::validateConfigurationForm($form, $form_state);
    $configuration = $form_state->getValues();

    // A d ACCOUNT connect is not yet implemented.
    if ($configuration['d_account_type'] === 'd_account') {
      $form_state->setError($form['d_account_type'], $this->t('d ACCOUNT connect is not supported yet.'));
    }

    // A domain is required if you use a dummy username and email address.
    if ($configuration['dummy_email_enable'] && empty($configuration['dummy_email_domain'])) {
      $form_state->setError($form['dummy_email_domain'], $this->t('Dummy email domain is a required field.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'client_id' => '',
      'client_secret' => '',
      'iss_allowed_domains' => 'id.smt.docomo.ne.jp',
      'scopes' => ['openid', 'email_n'],
      'authif' => 1,
      'dummy_email_domain' => 'example.com',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoints(): array {
    $endpoints = [];
    $endpoints['authorization'] = 'https://id.smt.docomo.ne.jp/cgi8/oidc/authorize';
    switch ($this->configuration['d_account_type']) {
      case 'business_d_account':
        // Endpoints of Business d ACCOUNT connect.
        $endpoints['token'] = 'https://conf.uw.docomo.ne.jp/token';
        $endpoints['userinfo'] = 'https://conf.uw.docomo.ne.jp/userinfo';
        $endpoints['end_session'] = 'https://id-biz.smt.docomo.ne.jp/cgi8/id/relogin';
        break;

      default:
        // Endpoints of d ACCOUNT connect.
        $endpoints['token'] = 'https://conf.uw.docomo.ne.jp/common/token';
        $endpoints['userinfo'] = 'https://conf.uw.docomo.ne.jp/common/userinfo';
        $endpoints['end_session'] = 'https://id.smt.docomo.ne.jp/cgi8/id/v2_0/relogin';
    }
    return $endpoints;
  }

  /**
   * {@inheritdoc}
   */
  public function retrieveUserInfo(string $access_token): ?array {
    $userinfo = parent::retrieveUserInfo($access_token);

    if (!is_array($userinfo)) {
      return NULL;
    }

    // Generate dummy username and e-mail address.
    if ($this->configuration['dummy_email_enable'] && !empty($this->configuration['dummy_email_domain']) && !empty($userinfo['email'])) {

      // Generate dummy username.
      $email = $userinfo['email'];
      $userinfo['name'] = explode('@', $email)[0] . '-' . md5($email);

      // Generate dummy e-mail.
      $email_domain = $this->configuration['dummy_email_domain'];
      $userinfo['email'] = $userinfo['name'] . '@' . $email_domain;
    }

    return $userinfo;
  }

  /**
   * {@inheritdoc}
   */
  protected function getUrlOptions(string $scope, GeneratedUrl $redirect_uri): array {
    $options = parent::getUrlOptions($scope, $redirect_uri);
    $options['query']['authif'] = $this->getAuthif();
    $options['query']['nonce'] = $this->generateNonce();
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getClientScopes(): ?array {
    // Get value from setting.
    return $this->configuration['scopes'] ?: [];
  }

  /**
   * Get an authif config.
   *
   * @return string
   *   authif parameter.
   */
  private function getAuthif(): string {
    return $this->configuration['authif'] ? '1' : '0';
  }

  /**
   * Generate a nonce.
   */
  private function generateNonce(): string {
    $nonce = md5(mt_rand());

    $request = $this->requestStack->getCurrentRequest();

    if ($request) {
      $session = $request->getSession();
      if ($session) {
        $session->remove('nonce');
        $session->set('nonce', $nonce);
      }
    }

    return $nonce;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl(array $route_parameters = [], array $options = []): Url {
    $redirect_uri = parent::getRedirectUrl($route_parameters, $options);

    // Only https can be used with d ACCOUNT connect.
    // If the scheme is http, change it to https.
    $urlString = $redirect_uri->toString();
    if (str_starts_with($urlString, 'http://')) {
      $urlString = 'https://' . substr($urlString, 7);
    }

    return Url::fromUri($urlString);
  }

  /**
   * {@inheritdoc}
   */
  protected function getRequestOptions(string $authorization_code, string $redirect_uri): array {
    $requestOptions = parent::getRequestOptions($authorization_code, $redirect_uri);

    // Only basic authentication can be used with d ACCOUNT connect.
    $requestOptions['headers']['Authorization'] = 'Basic ' . base64_encode($this->configuration['client_id'] . ':' . $this->configuration['client_secret']);

    return $requestOptions;
  }

}
