# d ACCOUNT OpenID Connect Client

The d ACCOUNT OpenID Connect client module provides a client for d ACCOUNT connect and Business d ACCOUNT connect as a provider of the [OpenID Connect](https://www.drupal.org/project/openid_connect) module.


## Table of contents (optional)

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:
- [OpenID Connect](https://www.drupal.org/project/openid_connect)


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to the openid_connect settings and enable the d ACCOUNT client at
   `Administration / Configuration / People / OpenID Connect clients`


## Maintainers

- Tomotaka Hosomi (`hosomitm`) (https://www.drupal.org/u/hosomitm)
- Yas Naoi (`yas`) (https://drupal.org/u/yas)
