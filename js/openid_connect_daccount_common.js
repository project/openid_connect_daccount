((Drupal) => {
  Drupal.behaviors.openidConnectDaccountCommon = {
    attach() {
      const dummyEmailEnable = document.getElementById(
        'edit-settings-dummy-email-enable',
      );
      const dummyEmailDomain = document.querySelector(
        'div.js-form-item-settings-dummy-email-domain',
      );

      function toggleDummyEmailDomainField() {
        dummyEmailDomain.style.display = dummyEmailEnable.checked
          ? 'block'
          : 'none';
      }
      toggleDummyEmailDomainField();
      dummyEmailEnable.addEventListener('change', toggleDummyEmailDomainField);
    },
  };
})(Drupal);
